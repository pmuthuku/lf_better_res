#!/bin/perl

use strict;
use warnings;

my @data=<STDIN>;

my $k;
my $max = 0;
my $x;

foreach $k(@data){
    chomp($k);

    if ( abs($k) > $max){
	$max = abs($k);
    }

}

#Checking for overflows and underflows
if ($max > 32767){
    foreach $k(@data){
	$x = ($k/($max+5))*32767;
	$x = sprintf("%d",$x);
	print "$x\n";
    }
}
else{
    foreach $k(@data){
	print "$k\n";
    }
}
