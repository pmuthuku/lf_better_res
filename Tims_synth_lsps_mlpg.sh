#!/bin/bash

#To be run in voice_directory/test/xxx/ directory

CG_TMP=jnk_$$
FLITEDIR=/home/pmuthuku/Dec11/flite/
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/pmuthuku/code/IAIF_test3/srlcode/lib/"

for i in arctic_a0[01]?9.mcep
do
    #Synthesizing with LSPs
    filenm=`basename $i .mcep`
    echo "Synthesizing $i"
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'if($F[1] >= 0.001){print "@F[1..21]";}' | x2x +a +f > $filenm.lsp
    lsp2lpc -i 1 -m 20 -s 16 $filenm.lsp | x2x +f +a | awk '{if (NR%21==0){print $0}else{printf("%f ",$1);}}' > $filenm.lpc
    perl -lane '$x = 400*(10**($F[0]/10)); $y = sprintf("%.3f",$x);print "$y";' < $filenm.lpc > $filenm.lpcp
    #awk '{print $1}'  < $filenm.lpc > $filenm.lpcp
    perl -ane '
for($j=1; $j < @F; $j++){                                                          
 $x= - $F[$j];                                                                    
 print "$x ";                                                                      
}                                                                                  
print "\n";' < $filenm.lpc > $filenm.lpcc
    paste $filenm.lpcp $filenm.lpcc > $filenm.lpcr
    $ESTDIR/bin/ch_track -itype  ascii -s 0.005 -c 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 -otype est_ascii $filenm.lpcr > $filenm.lpcs

    #This step overwrites a lot of the things in the previous steps
    $ESTDIR/bin/ch_track $i -c 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 -otype est_ascii -o $filenm.lsp
    /home/pmuthuku/code/IAIF_test2/fitlfm/est2fea_new $filenm.lsp $filenm.lspfea

    

    
    #LFM synthesis
    $ESTDIR/bin/ch_track $i -c 0 -otype est_ascii -o $CG_TMP.f0
    $ESTDIR/bin/ch_track $i -otype  est_ascii -c 43,44,45,46,47 -o $CG_TMP.lfmdimless
    $ESTDIR/bin/ch_track $i -otype  est_ascii -c 48,49,50,51,52,53 -o $CG_TMP.lfmlpc

    /home/pmuthuku/code/IAIF_test2/fitlfm/dimless2lf $CG_TMP.f0 $CG_TMP.lfmdimless $CG_TMP.lfm

    $ESTDIR/bin/ch_track $CG_TMP.lfm $CG_TMP.lfmlpc -otype est_ascii -pc first -o $CG_TMP.trk

    /home/pmuthuku/code/IAIF_test2/fitlfm/est2fea_src $CG_TMP.trk $CG_TMP.fea
    /home/pmuthuku/code/IAIF_test3/lfm2wav -gaf:-15 $CG_TMP.fea $filenm.res
    
    #cp $CG_TMP.wav tmp.wav 
    #$ESTDIR/bin/sigfilter tmp.wav -lpfilter 4000 -double -forder 101 -scaleN 0.65 -o $filenm.res
    
    #Synthesizing unvoiced sections
    #The below section looks hacky because there is a bug somewhere that trashes the times in the predicted track files. I will fix the bug later.
    #$ESTDIR/bin/ch_track $i -c 0 -otype ascii | $ESTDIR/bin/ch_track -itype ascii -s 0.005 -otype est_ascii -o $CG_TMP.v
    #Even though the help for the code below says LSP track, I really only use the power coefficient so it doesn't matter if I give it an LPC track instead
    #/home/pmuthuku/code/IAIF_test2/fitlfm/gen_unvoiced_res $filenm.lpcs $CG_TMP.v $filenm.uv
    #$ESTDIR/bin/ch_wave $filenm.uv -scale 0.1 -o $filenm.uva
    #/home/pmuthuku/code/IAIF_test2/fitlfm/voiced_unvoiced_res_comb $filenm.res $filenm.uva $CG_TMP.v $filenm.combwav



    #$FLITEDIR/testsuite/lpc_resynth -res $filenm.res -order 20 $filenm.lpcs ${filenm}.wav
    /home/pmuthuku/code/IAIF_test3/srlcode/lpcvoc -npass:4 $filenm.res $filenm.lspfea ${filenm}.wav

    #Scaling output
    $ESTDIR/bin/ch_wave ${filenm}.wav -scaleN 0.65 -o $filenm.wav

done
rm *.l*
rm $CG_TMP.*
#rm tmp.wav