#!/bin/bash

if [ ! -d ccoefs ]
then
    mkdir ccoefs
fi
CG_TMP=cg_tmp_$$

if [ "$PROMPTFILE" = "" ]
then
   if [ $# = 1 ]
   then
      PROMPTFILE=$1
   else
      PROMPTFILE=etc/txt.done.data
   fi
fi


cat $PROMPTFILE |
awk '{print $2}' |
while read i
do
    fname=$i
    echo $fname "COMBINE_COEFFS (f0,LSPs,LSP deltas,LF parameters,v)"
    $ESTDIR/bin/ch_track -otype ascii f0/$fname.f0 |
      awk '{if (NR == 1) { print $1; print $1} print $1}' >$CG_TMP.f0

    cat lsp/$fname.lsp > $CG_TMP.lsp
    cat lsp/$fname.lspd > $CG_TMP.lspd
    
    $ESTDIR/bin/ch_track -otype ascii lfm/$fname.lfm_dimless  > $CG_TMP.lfm_dimless


    if [ -f v/$fname.v ]
    then
	 cat v/$fname.v | awk '{print 10*$1}' > $CG_TMP.v
	 paste $CG_TMP.f0 $CG_TMP.lsp $CG_TMP.lspd $CG_TMP.lfm_dimless $CG_TMP.v
    else 
	paste $CG_TMP.f0 $CG_TMP.lsp $CG_TMP.lspd $CG_TMP.lfm_dimless
    fi |
    awk '{if (l==0) 
              l=NF;
            else if (l == NF)
              print $0}' |
    awk '{if (NR == -1) print $0; print $0}' |
    $ESTDIR/bin/ch_track -itype ascii -otype est_binary -s 0.005 -o ccoefs/$fname.mcep
    rm -f $CG_TMP.*
done
exit 0